import driverSetup.baseClass;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.Hrms.dashBoard;
import pages.Hrms.homeScreen;
import pages.Hrms.loginScreen;

import java.net.MalformedURLException;

public class loginTest extends baseClass {

    baseClass base;

    homeScreen home;

    loginScreen login;

    dashBoard dash;

    String userId = "employee001", password = "password";


    public loginTest() {

        base = PageFactory.initElements ( driver , baseClass.class );

    }

    @Test (priority = 1 )
    public  void login () throws MalformedURLException {

        home = base.setUp();

        login = home.clickLogin();

        dash = login.setLogin(userId,password);

        Assert.assertEquals ("Hi Emmanuel", dash.validateUser());
    }


}
