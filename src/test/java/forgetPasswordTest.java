import driverSetup.baseClass;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import pages.Hrms.forgotScreen;
import pages.Hrms.homeScreen;
import pages.Hrms.loginScreen;

import java.net.MalformedURLException;

public class forgetPasswordTest extends baseClass {

    baseClass base;

    homeScreen home;

    loginScreen login;

    forgotScreen pass;

    String emailID = "test@gmail.com";

    public forgetPasswordTest () {

        base = PageFactory.initElements ( driver , baseClass.class );
    }


    @Test (priority = 2 )
    public void recoverPassword () throws MalformedURLException {

        home = base.setUp();

        login = home.clickLogin();

        pass = login.passwordRecovery();

        pass.recoverPassword(emailID);
    }

}
