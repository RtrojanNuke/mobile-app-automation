package driverSetup;


import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import pages.Hrms.homeScreen;


import java.net.MalformedURLException;
import java.net.URL;

public class baseClass {

    /**
     * This class is meant for initializing and tearing-down driver.
     * Appium
     * Seedtest.io (cloud testing) accesskey is stored as a system variable
     *
     */



    String seedAccessKey = System.getenv("SEED_ACCESSKEY");

    protected AndroidDriver<AndroidElement> driver = null;

    DesiredCapabilities caps = new DesiredCapabilities();

    @BeforeClass
    public homeScreen setUp() throws MalformedURLException {

        caps.setCapability("testName", "Seamless HRMS Mobile App Automation");

        caps.setCapability("accessKey", seedAccessKey);

        caps.setCapability("deviceQuery", "@os='android' and @version='8.1.0' and @category='PHONE'");

        caps.setCapability(MobileCapabilityType.APP, "cloud:com.hc_hub/.MainActivity");

        caps.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.hc_hub");

        caps.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, ".MainActivity");

        driver = new AndroidDriver<>(new URL("https://cloud.seetest.io/wd/hub"), caps);

        return new homeScreen(driver);


    }

    @AfterClass
    public void tearDown()  {

        driver.quit();

        System.out.println("Report URL: "+ driver.getCapabilities().getCapability("reportUrl"));

    }
}

