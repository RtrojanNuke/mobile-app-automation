package pages.Hrms;

import driverSetup.constructor;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class homeScreen extends constructor {



    public homeScreen(AndroidDriver driver) {
        super(driver);
    }

    @FindBy( xpath = "//*[@text='SKIP']") WebElement skipToLogin;

    @FindBy ( xpath = "//*[@class='android.view.ViewGroup' and ./*[@text='Log in to your account']]") WebElement Login;


    public loginScreen clickLogin () {

        skipToLogin.click();

        Login.click();

        return new loginScreen(driver);
    }


}
