package pages.Hrms;

import driverSetup.constructor;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class forgotScreen extends constructor {


    public forgotScreen(AndroidDriver driver) {
        super(driver);
    }

    @FindBy (xpath = "//*[@text='Enter email']") WebElement recoveryEmail;

    @FindBy (xpath = "//*[@text='Submit']") WebElement Submit;


    public void recoverPassword (String email){

        recoveryEmail.sendKeys(email);

        Submit.click();
    }
}
