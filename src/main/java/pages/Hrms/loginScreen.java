package pages.Hrms;

import driverSetup.constructor;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class loginScreen extends constructor {

    public loginScreen(AndroidDriver driver) {

        super(driver);

    }



    @FindBy ( xpath = "((//*[@class='android.view.ViewGroup' and ./parent::*[@class='android.widget.ScrollView']]/*[@class='android.view.ViewGroup'])[2]/*/*[@class='android.widget.EditText'])[1]") WebElement InputEmail;

    @FindBy (xpath = "((//*[@class='android.view.ViewGroup' and ./parent::*[@class='android.widget.ScrollView']]/*[@class='android.view.ViewGroup'])[2]/*/*[@class='android.widget.EditText'])[2]") WebElement PWD;

    @FindBy (xpath = "//*[@text='Log in to your account']")WebElement signIn;

    @FindBy (xpath = "//*[@text='Forgot Password?']") WebElement forgotPassword;






    public dashBoard setLogin(String email, String password) {



        InputEmail.sendKeys(email);

        PWD.sendKeys(password);

        signIn.click();

        WebDriverWait wait = new WebDriverWait (driver, 10);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@text='Hi Emmanuel']")));

        return new dashBoard(driver);
    }

    public forgotScreen passwordRecovery() {

        forgotPassword.click();

        return new forgotScreen(driver);
    }
}
